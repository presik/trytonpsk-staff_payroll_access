# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
import datetime
from trytond.pyson import Id
from trytond.pool import PoolMeta, Pool

__all__ = ['Payroll']

_DEFAULT_WORK_DAY = 8


class Payroll:
    __metaclass__ = PoolMeta
    __name__ = 'staff.payroll'

    @classmethod
    def __setup__(cls):
        super(Payroll, cls).__setup__()

    def get_line_quantity(self, wage, start=None, end=None, extras=None, discount=None):
        Access = Pool().get('staff.access')
        config = Pool().get('staff.configuration')(1)
        quantity = wage.default_quantity or 0
        quantity_days = self.get_days(start, end)
        default_hour_workday = config.default_hour_workday or _DEFAULT_WORK_DAY

        #Se le paga al trabajador subsidio de alimentacion los dias no laborados
        if wage.synchronize_access or wage.type_concept == 'food':
            enter_timestamp = datetime.datetime.combine(start, datetime.time(0, 0))
            exit_timestamp = datetime.datetime.combine(end, datetime.time(23, 59))
            accesses = Access.search([
                ('employee', '=', self.employee.id),
                ('enter_timestamp', '>=', enter_timestamp),
                ('enter_timestamp', '<=', exit_timestamp),
            ])
            if wage.type_concept == 'food' and wage.synchronize_access:
                quantity_days = len(accesses)
            else:
                quantity_days = quantity_days - len(accesses)

        # Condicion para cuando el trabajador inicia contrato y no trabaja
        # el total de horas en la semana o el total de dias de la
        # quincena, entonces se descuenta parcialmente el domingo, en
        # caso de que haya trabajado menos dias del periodo
        # de liquidacion por defecto.
        elif wage.type_concept == 'salary' and self.employee.contract \
            and self.employee.position and self.employee.position.partial_sunday:
            monday = 0
            sunday = 6
            contract_start = self.employee.contract.start_date
            contract_end = self.employee.contract.end_date
            if contract_start >= start and contract_start <= end and \
                contract_start.weekday() not in [monday, sunday] and \
                quantity_days < config.default_liquidation_period:
                # Verifica si en los dias trabajados hay un domingo
                # para descontarlo parcialmente
                days_week = [(start + datetime.timedelta(d)).weekday()
                        for d in range(quantity_days)]
                if sunday in days_week:
                    dom_partial = (6 - contract_start.weekday()) * 0.16666
                    quantity_days = Decimal(str(round((quantity_days - 1 + dom_partial), 2)))

            if contract_end:
                if contract_end >= start and contract_end <= end and \
                    contract_end.weekday() not in [sunday,] and \
                    quantity_days < config.default_liquidation_period:
                    dom_partial = (1 + contract_end.weekday()) * 0.16666
                    quantity_days = Decimal(str(round((float(quantity_days) + dom_partial), 2)))
        elif wage.synchronize_access:
            quantity_days = Decimal(str(round((float(quantity_days) + dom_partial), 2)))

        quantity = self._get_line_quantity(quantity_days, wage, extras, discount)
        return quantity
