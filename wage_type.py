#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


__all__ = ['WageType']


class WageType:
    __metaclass__ = PoolMeta
    __name__ = 'staff.wage_type'
    synchronize_access = fields.Boolean('Synchronize Access')

    @classmethod
    def __setup__(cls):
        super(WageType, cls).__setup__()
