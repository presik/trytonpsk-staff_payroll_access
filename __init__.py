# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .payroll import Payroll
from .wage_type import WageType


def register():
    Pool.register(
        WageType,
        Payroll,
        module='staff_payroll_access', type_='model')
